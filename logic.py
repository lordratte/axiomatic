import sys
import itertools

from functools import reduce
from numbers import Number

SYMBOLS = {
    'not': '~',
    'and': '&',
    'or': '|',
    'xor': '^',
    'if': '>',
    'iff': '=='
}

UNARY = ('not',)

class Term:
    def __init__(self, single=None, parts=None, op=None):
        if not any(a is not None for a in (single, parts, op)):
            return
        if not ((single is None and parts is not None and op is not None) or (single is not None and parts is None and op is None)):
            raise TypeError('{cls} must either have a single variable/literal, or parts and an operator, or must be empty')
        if single is None:
            self._simple = False
            self._parts = {}
            if len(parts) > 1:
                self._parts['left'] = parts[0]
                self._parts['right'] = parts[1]
            else:
                self._parts['right'] = parts[0]
            for k,v in self._parts.items():
                v.parent = self
            self._op = op
        else:
            if isinstance(single, bool):
                single = int(single)
            if isinstance(single, Number) and not(single <= 1 and single >= 0):
                raise ValueError('Numerical input must be greater than or equal to 0 and less than or equal to 1')

            self._simple = True
            self._value = single
            self.parent = None

    
    def _copy(self, to=None):
        if self.type() in ('var', 'const'):
            new = type(self)(self._value)
        else:
            new = type(self)()
            new._parts = self._parts.copy()
            new._simple = self._simple
            new._op = self._op
        if to is not None:
            new.parent = to
        return new

    def copy(self):
        new_tree = self._copy()
        queue = [new_tree]

        while len(queue):
            cur = queue.pop(0)
            if cur.type() in ('var', 'const'):
                pass
            else:
                for key,side in cur._parts.items():
                    cur._parts[key] = side._copy(cur)
                    queue.append(cur._parts[key])

        return new_tree


    def type(self):
        if not self._simple:
            return 'expr'
        elif isinstance(self._value, str):
            return 'var'
        else:
            return 'const'
    
    def val(self):
        this = self.solve()
        if this.type() == 'expr':
            return None
        return this._value

    def op(self):
        if hasattr(self, '_op'):
            return self._op
        return None

    def left(self, val=None):
        if self.type() in ('var', 'const'):
            raise Exception('Value is simple.')
        if val is None:
            return self._parts.get('left')
        else:
            self._parts['left'] = val

    def right(self, val=None):
        if self.type() in ('var', 'const'):
            raise Exception('Value is simple.')
        if val is None:
            return self._parts.get('right')
        else:
            self._parts['right'] = val

    @classmethod
    def _grow(cls, op, left=None, right=None):
        parts = []
        if left is not None:
            parts.append(left)
        if right is not None:
            parts.append(right)
        return cls(parts=tuple(parts), op=op)

    def __repr__(self):
        clsname = type(self).__name__
        if self.type() in ('var', 'const'):
            return '{cls}({val})'.format(cls=clsname, val=repr(self.val()))
        return self._to_text(True, repr(self._parts.get('left')), repr(self._parts['right']))

    def __str__(self):
        if self.type() in ('var', 'const'):
            return str(self.val())
        return self._to_text(False, str(self._parts.get('left')), str(self._parts['right']))

    def _to_text(self, raw=False, left=None, right=None):
        clsname = type(self).__name__
        format_two, format_three = ('({o}{r})', '({l} {o} {r})') if not raw else (clsname+'._("{o}", {r})', clsname+'._({l}, "{o}", {r})')
        if self._op in UNARY:
            return format_two.format(o=SYMBOLS[self._op], r=right)
        else:
            return format_three.format(l=left, o=SYMBOLS[self._op], r=right)

    def variables(self):
        root = ({'root': self}, 'root')

        queue = [root]
        collect = {}

        while len(queue):
            par, par_key = queue.pop(0)
            if par[par_key].type() in ('var', 'const'):
                if par[par_key].type() == 'var':
                    instances = collect[par[par_key]._value] = collect.get(par[par_key]._value, [])
                    instances.append(par[par_key])
            else:
                cur = par[par_key]._parts
                for key,side in cur.items():
                    queue.append((cur, key))

        return collect

    def solve(self):
        this = self.eliminate()
        solution = this
            
        if this.type() == 'expr':
            left = this.left()
            right = this.right()
            if left is not None:
                left = left.solve()
            if right is not None:
                right = right.solve()

            if right is not None and right.type() == 'const':
                right = right.val()
                if this._op not in UNARY:
                    if (left is not None and left.type() == 'const'):
                        left = left.val()
                        if this._op == 'and':
                            solution =  this._solve_and(left, right)
                        elif this._op == 'or':
                            solution = this._solve_or(left, right)
                        elif this._op == 'xor':
                            solution = this._solve_xor(left, right)
                        elif this._op == 'if':
                            solution = this._solve_if(left, right)
                        elif this._op == 'iff':
                            solution = this._solve_iff(left, right)
                else:
                    if this._op == 'not':
                        solution = this._solve_not(right)
            else:
                this.left(left)
                this.right(right)
        return solution

    def eliminate(self):
        if len(self.variables()):
            andres = reduce(lambda a, b: a & b, (w[1] for w in self.worlds())).solve()
            if andres.val() > 0:
                return andres
            orres = reduce(lambda a, b: a | b, (w[1] for w in self.worlds())).solve()
            if orres.val() == 0:
                return orres
        return self

    def suppose(self, **assumptions):
        hyp = self.copy()
        variables = hyp.variables()
        for var,terms in variables.items():
            if var in assumptions:
                for term in terms:
                    term._value = assumptions[var]
        return hyp

    def worlds(self):
        variables = sorted(list(set(self.variables().keys())))
        worlds = itertools.product([0, 1], repeat=len(variables))
        
        for world in worlds:
            assumption = dict(zip(variables, world))
            yield world, self.suppose(**assumption)
        return

    def table(self):
        variables = sorted(list(set(self.variables().keys())))
        print('{0} | {1}'.format('\t'.join(variables), str(self)))
        for w in self.worlds():
            print('{0} | {1}'.format('\t'.join(map(str, w[0])), w[1].solve()))

    @classmethod
    def _(cls, *args):
        if len(args) not in (2, 3):
            raise TypeError('Too many arguments. Must be either 2 or 3')
        clean_parts = lambda *parts: tuple(cls(t) if any(isinstance(t, typ) for typ in (Number, str)) else t for t in (parts))
        if len(args) == 2:
            op = next(k for k,v in SYMBOLS.items() if v == args[0])
            return cls(parts=clean_parts(args[1]), op=op)
        else:
            op = next(k for k,v in SYMBOLS.items() if v == args[1])
            return cls(parts=clean_parts(args[0], args[2]), op=op)

    @staticmethod
    def _validate_number_args(*args):
        if any(not isinstance(a,Number) for a in args):
            raise TypeError('Args must all be numbers: {}'.format(args))
    
    @classmethod
    def _validate_class_args(cls, *args):
        if any(not isinstance(a,cls) for a in args):
            raise TypeError('Args must all be of type {}: {}'.format(cls.__name__, args))

    def __int__(self):
        return int(bool(self))

    def __float__(self):
        val = self.val()
        if val is None or isinstance(val, str):
            raise ValueError("Expression isn't a tautology or paradox. Cannot reduce.")
        return float(val)

    def __bool__(self):
        val = self.val()
        if val is None or isinstance(val, str):
            raise ValueError("Expression isn't a tautology or paradox. Cannot reduce.")
        return val != 0

    @classmethod
    def _solve_if(cls, left, right):
        cls._validate_number_args(left, right)
        return cls._solve_or(cls._solve_not(left).val(), right)
    @classmethod
    def _solve_iff(cls, left, right):
        cls._validate_number_args(left, right)
        return cls._solve_and(cls._solve_if(left, right).val(), cls._solve_if(right, left).val())
    @classmethod
    def _solve_xor(cls, left, right):
        cls._validate_number_args(left, right)
        return cls._solve_or(cls._solve_and(cls._solve_not(left).val(), right).val(), cls._solve_and(left, cls._solve_not(right).val()).val())

    @classmethod
    def _solve_not(cls, right):
        cls._validate_number_args(right)
        return cls(1 - right)

    @classmethod
    def _solve_and(cls, left, right):
        cls._validate_number_args(left, right)
        return cls(min(left, right))

    @classmethod
    def _solve_or(cls, left, right):
        cls._validate_number_args(left, right)
        return cls(max(left, right))

    def __and__(self, other):
        self._validate_class_args(self, other)
        return self._grow('and', self, other)
    def __or__(self, other):
        self._validate_class_args(self, other)
        return self._grow('or', self, other)
    def __xor__(self, other):
        self._validate_class_args(self, other)
        return self._grow('xor', self, other)
    def __gt__(self, other):
        self._validate_class_args(self, other)
        return self._grow('if', self, other)
    def __eq__(self, other):
        self._validate_class_args(self, other)
        return self._grow('iff', self, other)
    def __invert__(self):
        self._validate_class_args(self)
        return self._grow('not', right=self)

