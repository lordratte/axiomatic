**Warning: do not try this unless you understand what you are doing**

If you do not like the builtin choice of operators, you may configure custom symbols for yourself. This is un-endorsed, un-recommended and un-tested.

Wherever you import the `Term` object, there are a couple of ways you can try to change it. For instance, let's suppose you don't like the if symbol and want to replace it with the right shift. At the top of your programme, write this:

```python
from axiomatic import Term, SYMBOLS
del Term.__gt__
class Term(Term):
	def __rshift__(self, other):
		self._validate_class_args(self, other)
		return self._grow('if', self, other)
SYMBOLS['if'] = '>>'
```
Or something like this:
```python
from axiomatic import Term, SYMBOLS
Term.__rshift__ = Term.__gt__
del Term.__gt__
SYMBOLS['if'] = '>>'
```

In both of these cases, you could now write `Term(1) >> Term(0)` and it would now mean what `Term(1) > Term(0)` used to mean.

Remember that changing this will change the order of operations. This is fine as long as you explicitly parenthesise.
