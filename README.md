[![coverage report](https://gitlab.com/lordratte/axiomatic/badges/master/coverage.svg)](https://gitlab.com/lordratte/axiomatic/-/commits/master)
[![pipeline status](https://gitlab.com/lordratte/axiomatic/badges/master/pipeline.svg)](https://gitlab.com/lordratte/axiomatic/-/commits/master) 
# NB
1. This code has no warranty et al. Don't use it for important stuff.

2. The code is free to use for anything that is honourable, just, and pure. Acknowledgements can be made to gitlab.com/lordratte

# Axiomatic
Axiomatic is a lightweight library to make working with formal logic and fuzzy logic more intuitive.

# Usage

## Basics
### Create Terms
```python
from axiomatic import Term

a = Term('a')
b = Term('b')


true = Term(1)
also_true = Term(True)
false = Term(0)
kinda = Term(0.6)
```

### Basic Expressions
```python
>>> expr = true & true
>>> print(expr)
(1 & 1)

>>> expr
Term._(Term(1), "&", Term(1))
```
**NB: for a full list of operations, see [the table](#table-of-operations) later in this file.**


### Reduce expressions
```python
>>> expr.solve()
Term(1)
```

### Convert terms to other types
**NB: notice how the uncertain fuzzy logic value 0.6 is rounded up when being converted to bool and int**
```python
>>> int(expr.solve())
1
>>> int(kinda)
1
>>> float(kinda)
0.6
>>> bool(kinda)
True
>>> bool(false)
False
>>> float(false)
0.0
```

### Fuzzy logic operations
```python
>>> (kina & true).solve()
Term(0.6)
>>> (kina | true).solve()
Term(1)
>>> (kina & false).solve()
Term(0)
>>> (kina | false).solve()
Term(0.6)
```

### Value function

In the above examples we used `.solve()` but we can also use `.val()` to get the underlying value. If a system can't be solved with the given information, val will return `None` (unlike calling `bool`, `int` or `float` on it which throws an error).
```python
>>> Term(1).val()
1
>>> (a | ~a).val() # This is true because it's a tautology. Every row of the truth-table is true.
1
>>> print((a | a).val()) # This is None because, although there are some true rows in the truth table, there are also some false ones.
None
>>> a.val() # Even though this would technically be None because it can't be calculated on its own, it returns the variable name out of convenience because it is of type 'var' and not 'expr' (see more on types later).
'a'
>>> (a & ~a).val()  # This 
0
```

## Other Functions

### Print a Truth-table
```python
>>> (a & b).table()
a	b | (a & b)
0	0 | 0
0	1 | 0
1	0 | 0
1	1 | 1
```

### Get a Truth-table as a Generator
*Notice how the first state corresponds with the first row of the truth table*
```python
>>> possible_states = (a & b).worlds()
>>> first_state = next(possible_states)
>>> first_state[0] # Variable configuration for row
(0, 0)
>>> first_state[1] # Unsolved result of row's variables applied to expression
Term._(Term(0), "&", Term(0))
>>> first_state[1].solve() # Solved result of row's variables applied to expression
Term(0)
```

### Type
```python
>>> Term('a').type() # Variables
'var'
>>> Term(1).type() # Constants
'const'
>>> (Term('a') & Term(1)).type() #Expressions
'expr'
```

### Copy
Sometimes you'll want to make a deep copy of system.
```python
>>> expr = Term('a') | Term('b')
>>> expr is expr
True
>>> expr is expr.copy()
False
```

### Left and Right
If a system is of type 'expr' then `.left()` and `.right()` will return the respective side of an expression as a term (`.left()` returns `None` for unary operators like "~", i.e. not, because only the right side is used).

```python
>>> (Term('a') & Term(1)).left()
Term('a')
>>> (Term('a') & Term(1)).right()
Term(1)
>>> (~Term('a')).left()
>>> (~Term('a')).right()
Term('a')
```
However, if the system is of type 'var' or 'const', it will raise an error if you try to get a side because they simply represent single scalar values.

Left and right can also be used to replace terms.
```python
>>> expr = Term('a') ^ Term(1)
>>> expr
Term._(Term('a'), "^", Term(1))
>>> expr.left(Term(0))
>>> expr
Term._(Term(0), "^", Term(1))
```

If you want to leave the original expression unchanged, make a copy.
```python
>>> expr = Term('a') ^ Term(1)
>>> expr2 = expr.copy()
>>> expr2.left(Term(1))
>>> expr2
Term._(Term(1), "^", Term(1))
>>> expr
Term._(Term('a'), "^", Term(1))
```

### Operator
To get the name of the operator use the `.op()` function.
```python
>>> expr.op()
'xor'
>>> from axiomatic import SYMBOLS
>>> SYMBOLS[expr.op()]
'^'
```

Is an operator unary?   
```python
>>> from axiomatic import UNARY
>>> expr.op() in UNARY
False
>>> (~Term('a')).op() in UNARY
True

```

### Variables

You can get a dictionary of all the variables with the `.variables()` function. Each key is the name of the variable. Each value is a list of strong references to all the instances of that variable in the relevant expression.

**NB: In other words, if you changes these values, you will change the expression. If you just want to test values, use the suppose() function.**

```python
>>> expr = ((~((Term('a') & Term('b'))) ^ Term('a')))
>>> expr.variables()
{'a': [Term('a'), Term('a')], 'b': [Term('b')]}
```

### Suppose
With any system, sometimes we'll want to make a hypothesis over it. We can use the `.suppose()` function for that.

Let's take a non-tautological statement a as follows
```python
>>> expr = ((~((Term('a') & Term('b'))) ^ Term('a')))
>>> expr.table()
a	b | ((~(a & b)) ^ a)
0	0 | 1
0	1 | 1
1	0 | 0
1	1 | 1
```
Let's suppose that a is false.
```python
>>> print(expr.suppose(a=0).val())
1
```
In this case, the resultant system is now true because we have factored out the situations that prevented the system from being a tautology. In other words, explaining it visually...
```python
>>> expr.suppose(a=0).table()
b | ((~(0 & b)) ^ 0)
0 | 1
1 | 1
```
Likewise, this is indeterminate...
```python
>>> print(expr.suppose(a=1).val())
None
```
Because
```python
>>> expr.suppose(a=1).table()
b | ((~(1 & b)) ^ 1)
0 | 0
1 | 1
```

**NB: suppose() leaves the original system unchanged. Calling it with no arguments is the same as calling copy()**

# Table of Operations
In order from lowest precedence (least binding) to highest precedence (most binding). Except for "if" and "iff" which are evaluated from left to right.

*Note: if you absolutely have to replace one of these operators, try [this](./docs/custom_operators.md) experimental suggestion for patching it (keeping in mind that it might break things like the order of operations).*

| Logic Operator      | Symbol | Example                  | Rough Python Equivalent           |
| :-----------------: | :----: | :----------------------: | :-------------------------------: |
| IFF [^1]            | ==     | `Term('a') == Term('b')` |  `a == b`                         |
| IF [^1]             | >      | `Term('a') > Term('b')`  |  `b if a else True`               |
| OR                  | \|     | `Term('a') \| Term('b')` |  `a or b`                         |
| XOR                 | ^      | `Term('a') ^ Term('b')`  |  `(a and not b) or (not a and b)` |
| AND                 | &      | `Term('a') & Term('b')`  |  `a and b`                        |
| NOT                 | ~      | `~Term('b')`             |  `not b`                          |


[^1]: If you are using "iff" or "if" with compound expressions, make sure to parenthesize both sides. Python can short-circuit equality operators (which will drop a chunk off your expression).
