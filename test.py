#!/usr/bin/env python
import unittest

from logic import Term, UNARY

OPS = {
    'and': (lambda a, b: a & b, lambda a, b: a and b),
    'or': (lambda a, b: a | b, lambda a, b: a or b),
    'xor': (lambda a, b: a ^ b, lambda a, b: (a and not b) or (not a and b)),
    'if': (lambda a, b: a > b, lambda a, b: not a or b),
    'iff': (lambda a, b: a == b, lambda a, b: a is b),
    'not': (lambda a, b: ~ b, lambda a, b: not b)
}

class TestTerm(unittest.TestCase):
    def setUp(self):
        self.a = Term('a')
        self.b = Term('b')

    def test_constructor(self):
        self.assertRaises(Exception, Term, 'a', parts=(self.a, self.b), op='&')
        self.assertRaises(Exception, Term, 'a', parts=(self.a, self.b))
        self.assertRaises(ValueError, Term, 1.5)
        self.assertRaises(ValueError, Term, -1)
        self.assertEqual(Term(0).val(), 0)
        self.assertEqual(Term(1).val(), 1)
        self.assertEqual(Term(True).val(), 1)
        self.assertEqual(Term(False).val(), 0)

    def test_conversion(self):
        self.assertRaises(ValueError, int, self.a)
        self.assertRaises(ValueError, float, self.a)
        self.assertRaises(ValueError, bool, self.a)
        self.assertEqual(int(Term(0.5)), 1)
        self.assertEqual(float(Term(0.5)), 0.5)
        self.assertIs(bool(Term(0.5)), True)

        self.assertEqual(str(self.a), 'a')
        self.assertEqual(str(self.a | self.b), '(a | b)')
        self.assertEqual(str(~self.a), '(~a)')
        self.assertEqual(str(eval(repr(self.a ^ self.b))), str(self.a ^ self.b))

    def test_operators(self):
        for op in OPS:
            print(op)
            self.subTest(op)
            for a in (False, True):
                for b in (False, True):
                    av,bv = int(a), int(b)
                    self.assertIs(int(OPS[op][0](Term(av), Term(bv)).solve()),int(OPS[op][1](a, b)))
                    if op not in UNARY:
                        self.assertRaises(Exception, OPS[op][0], Term(av), 1)
                        self.assertRaises(Exception, OPS[op][0], 1, Term(av))

    def test_general_methods(self):
        e = (self.a & self.b)
        self.assertIsNot(e, e.copy())
        self.assertEqual(repr(e), repr(e.copy()))

        self.assertIs(e.left(), self.a)
        self.assertIs(e.right(), self.b)
        self.assertIsNone((~self.a).left())
        self.assertRaises(Exception, self.a.left)
        self.assertRaises(Exception, self.a.right)

        e.left(Term(1))
        e.right(Term(1))
        self.assertEqual(int(e.solve()), 1)

        self.assertEqual(sorted((self.a & self.b).variables().keys()), ['a', 'b'])

        self.assertEqual((Term(1) & (Term('b') | Term('b'))).solve().type(), 'expr')

        self.assertEqual(float((self.a > self.b).suppose(a=1, b=0).solve()), 0)

        self.assertEqual(len(list((Term('a') & Term('b')).worlds())), 4)
        self.assertEqual(len(list((Term('a') & Term('b')).worlds())[0]), 2)

        self.a.table()

        self.assertRaises(Exception, Term._, 'a')
        self.assertRaises(Exception, Term._, 'a', '&', 'b', 'extra info')
        self.assertEqual(Term._('~', 1).val(), 0)
        self.assertEqual(Term._(1, '>', 0).val(), 0)
        self.assertIsNone((Term('a') | Term('a')).val())

        self.assertRaises(Exception, Term._solve_if, Term(1), 1)

        
        self.assertIsNone(Term(1).op())
        self.assertEqual((Term('a') > Term(1)).op(), 'if')

    
    def test_axioms_on_variables(self):
        # Commutativity
        self.assertEqual(int(Term('p') & Term('q') == Term('q') & Term('p')), 1)
        self.assertEqual(int(Term('p') | Term('q') == Term('q') | Term('p')), 1)
        # This next one needs to be parenthesized otherwise terms drop away
        #self.assertEqual(int(Term('p') == Term('q') == Term('q') == Term('p')), 1)
        self.assertEqual(int((Term('p') == Term('q')) == (Term('q') == Term('p'))), 1)

        # Associativity
        self.assertEqual(int(Term('p') & (Term('q') & Term('r')) == (Term('p') & Term('q')) & Term('r')), 1)
        self.assertEqual(int(Term('p') | (Term('q') | Term('r')) == (Term('p') | Term('q')) | Term('r')), 1)

        # Distributivity
        self.assertEqual(int(Term('p') | (Term('q') & Term('r')) == (Term('p') | Term('q')) & (Term('p') | Term('r'))), 1)
        self.assertEqual(int(Term('p') & (Term('q') | Term('r')) == (Term('p') & Term('q')) | (Term('p') & Term('r'))), 1)

        # De Morgan
        self.assertEqual(int(~(Term('p') & (Term('q')) == ~Term('p') | ~Term('q'))), 1)
        self.assertEqual(int(~(Term('p') | (Term('q')) == ~Term('p') & ~Term('q'))), 1)

        # Negation
        self.assertEqual(int(~~Term('p') == Term('p')), 1)

        # Excluded Middle
        self.assertEqual(int(~Term('p') | Term('p')), 1)

        # Contradiction
        self.assertEqual(int(~Term('p') & Term('p')), 0)

        # Implication
        self.assertEqual(int((~Term('p') | Term('q')) == (Term('p') > Term('q'))), 1)

        # Equality
        # Equality seems to significantly drop away terms as a semantic aspect of python
        # parenthesis or variable grouping is needed
        #self.assertEqual(int(Term('p') == Term('p') == (Term('p') > Term('q')) & (Term('q') > Term('p'))), 1)
        lhs = Term('p') == Term('q')
        rhs = (Term('p') > Term('q')) & (Term('q') > Term('p'))
        self.assertEqual(int(lhs == rhs), 1)

        #or-absorbtion
        self.assertEqual(int(Term('p') | Term('p') == Term('p')), 1)
        self.assertEqual(int(Term('p') | Term(1) == Term(1)), 1)
        self.assertEqual(int(Term('p') | Term(0) == Term('p')), 1)
        self.assertEqual(int(Term('p') | (Term('p') & Term('q')) == Term('p')), 1)

        #or-absorbtion
        self.assertEqual(int(Term('p') & Term('p') == Term('p')), 1)
        self.assertEqual(int(Term('p') & Term(1) == Term('p')), 1)
        self.assertEqual(int(Term('p') & Term(0) == Term(0)), 1)
        self.assertEqual(int(Term('p') & (Term('p') | Term('q')) == Term('p')), 1)

        # Identity
        self.assertEqual(int(Term('p') == Term('p')), 1)




def suite():
    'Test Suite'

    suite = unittest.TestSuite()

    suite.addTests(
        unittest.TestLoader().loadTestsFromTestCase(TestTerm)
    )

    return suite

 

if __name__ == '__main__':
    result = unittest.TextTestRunner(verbosity=2).run(suite())
    exit(0) if result.wasSuccessful() else exit(1)

